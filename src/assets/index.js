const MarkIcon = require('./image/Mark.png');
const FingerPrintIcon = require('./image/fingerPint.png');
const Menu = require('./image/main.png');
const Account = require('./image/acount.png');
const Product1 = require('./image/product1.png');
const ProductBig = require('./image/productBig.png');
const productDetail = require('./image/productDetail.png');
const AsaroooImage = require('./image/Asarooo.png');
const EforRiroImage = require('./image/EforRiro.png');
const chieckenStewImage = require('./image/chieckenStew.png');
const moimoiImage = require('./image/moimoiImage.png');

export {
  MarkIcon,
  FingerPrintIcon,
  Menu,
  Account,
  Product1,
  ProductBig,
  productDetail,
  AsaroooImage,
  EforRiroImage,
  chieckenStewImage,
  moimoiImage,
};
