import {
  AsaroooImage,
  EforRiroImage,
  Product1,
  chieckenStewImage,
  moimoiImage,
} from '../assets';

export const productList = [
  {
    productName: 'African Doughnut Mix',
    productImage: Product1,
    productPrice: '£30',
  },
  {
    productName: 'Efo Riro',
    productImage: EforRiroImage,
    productPrice: '£30',
  },
  {
    productName: 'Asaro (Yam Porridge)',
    productImage: AsaroooImage,
    productPrice: '£30',
  },
  {
    productName: 'Chicken Stew',
    productImage: chieckenStewImage,
    productPrice: '£30',
  },
  {
    productName: 'Asaro (Yam Porridge)',
    productImage: AsaroooImage,
    productPrice: '£30',
  },
  {
    productName: 'Asaro (Yam Porridge)',
    productImage: AsaroooImage,
    productPrice: '£30',
  },
];
export const cartList = [
  {
    productName: 'Asaro',
    productType: '(Yam Porridge)',
    productImage: AsaroooImage,
    productPrice: '£30',
    quantityPurchased: 2,
  },
  {
    productName: 'Moi Moi ',
    productType: '(Bean Cake)',
    productImage: moimoiImage,
    productPrice: '£30',
    quantityPurchased: 1,
  },
  {
    productName: 'Efo Riro',
    productImage: EforRiroImage,
    productType: '',

    productPrice: '£30',
    quantityPurchased: 1,
  },
];
